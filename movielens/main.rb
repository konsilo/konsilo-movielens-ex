# encoding: UTF-8

require 'rubygems'
require 'securerandom'
require File.expand_path('../konsilo/config/environment')


def create_items
  file = File.foreach('data/u.item')
  failures = []

  file.each do |line|
    information = line.force_encoding("iso-8859-1").split("|")
    categories = {
      5 => "unknown",
      6 => "action",
      7 => "adventure",
      8 => "animation",
      9 => "children",
      10 => "comedy",
      11 => "crime",
      12 => "documentary",
      13 => "drama",
      14 => "fantasy",
      15 => "film-noir",
      16 => "horror",
      17 => "musical",
      18 => "mystery",
      19 => "romance",
      20 => "sci-fi",
      21 => "thriller",
      22 => "war",
      23 => "western",
    }

    alert = Alert.new

    alert.id = information[0]
    alert.title = information[1]

    tags = []
    categories.each do |key, value|
      if information[key] == "1"
        tags << value
      end
    end


    alert.category_list.add(tags)
    if alert.save
      print "."
    else
      print "f"
      failures << alert
    end
  end

  puts "\nCreated alerts: #{Alert.count}"
  puts "Failures #{failures.count}: ", failures
end

def create_users
  total = File.foreach('data/u1.base').count
  email = "user@something.com"
  password = SecureRandom.hex(6)
  total.times do |i|
    if User.create(id: i+1, email: ((i+1).to_s + email), password: password)
      print "."
    else
      print "f"
    end
  end

  puts "Users: #{User.count}"
end

def create_training_data
  file = File.foreach('data/u1.base')
  failures = []
  user = nil

  file.each_with_index do |line, i|
    begin
      information = line.split(" ")

      user_id = information[0]
      alert_id = information[1]
      like = like_rating(information[2])

      if user.blank? || (user_id.to_i != user.id)
        user = User.find(user_id.to_i)
      end

      alert = Alert.find alert_id.to_i

      if like
        user.liked alert
      else
        user.disliked alert
      end

      print "."
    rescue
      print "f"
      failures << i
    end
  end

  puts "\nVotes: #{ActsAsVotable::Vote.count}"
  puts "Failures #{failures.count}: ", failures
end

def update_recommendations
  n = 10

  User.all.each do |user|
    user.extend CollaborativeFilteringRecommenderHelper
    user.extend ContentBasedRecommenderHelper

    begin
      collaborative = user.get_high_collaborative_filtering_scores(n)
      content_based = user.get_high_content_based_scores(n)

      print "."
    rescue
      print "f"
    end
  end
end

def compute_precision(n = 10)

  file = File.new("file_n_#{n}.txt", "w")
  file.puts("#{n}")

  test = File.foreach('data/u1.test')
  failures = []
  user = nil

  file.puts("Computed for:")

  total_collaborative_tp = 0
  total_collaborative_tn = 0
  total_collaborative_fp = 0
  total_collaborative_fn = 0

  total_content_tp = 0
  total_content_tn = 0
  total_content_fp = 0
  total_content_fn = 0

  total = 0

  collaborative_tp = 0
  collaborative_tn = 0
  collaborative_fp = 0
  collaborative_fn = 0

  content_tp = 0
  content_tn = 0
  content_fp = 0
  content_fn = 0


  test.each_with_index do |line, i|
    begin
      information = line.split(" ")

      user_id = information[0]
      alert_id = information[1]
      like = like_rating(information[2])

      if user.blank? || (user_id.to_i != user.id)
        unless user.blank?
          file.puts("Collaborative ")
          file.puts("TP #{collaborative_tp}")
          file.puts("TN #{collaborative_tn}")
          file.puts("FP #{collaborative_fp}")
          file.puts("FN #{collaborative_fn}")

          file.puts("Content ")
          file.puts("TP #{content_tp}")
          file.puts("TN #{content_tn}")
          file.puts("FP #{content_fp}")
          file.puts("FN #{content_fn}")
        end

        collaborative_tp = 0
        collaborative_tn = 0
        collaborative_fp = 0
        collaborative_fn = 0

        content_tp = 0
        content_tn = 0
        content_fp = 0
        content_fn = 0

        user = User.find(user_id.to_i)

        file.puts("#{user_id}")
      end

      alert = Alert.find alert_id.to_i

      scores = user.collaborative_filtering_scores.limit(n).order("score desc")
      collaborative = []

      scores.each do |score|
        collaborative << Alert.find(score.alert_id)
      end

      scores = user.content_based_scores.limit(n).order("score desc")
      content = []

      scores.each do |score|
        content << Alert.find(score.alert_id)
      end

      #tp = tp
      #tn = fn
      #fn = tn
      #fp = fp
      if like
        if collaborative.include?(alert)
          collaborative_tp = collaborative_tp + 1
          total_collaborative_tp = total_collaborative_tp + 1
        else
          collaborative_tn = collaborative_tn + 1
          total_collaborative_tn = total_collaborative_tn + 1
        end

        if content.include?(alert)
          content_tp = content_tp + 1
          total_content_tp = total_content_tp + 1
        else
          content_tn = content_tn + 1
          total_content_tn = total_content_tn + 1
        end
      else
        if collaborative.include?(alert)
          collaborative_fp = collaborative_fp + 1
          total_collaborative_fp = total_collaborative_fp + 1
        else
          collaborative_fn = collaborative_fn + 1
          total_collaborative_fn = total_collaborative_fn + 1
        end

        if content.include?(alert)
          content_fp = content_fp + 1
          total_content_fp = total_content_fp + 1
        else
          content_fn = content_fn + 1
          total_content_fn = total_content_fn + 1
        end
      end

      total = total + 1

      print "."

    rescue
      print "f"
      failures << i
    end
  end

  file.puts("Total Collaborative ")
  file.puts("TP #{total_collaborative_tp}")
  file.puts("TN #{total_collaborative_tn}")
  file.puts("FP #{total_collaborative_fp}")
  file.puts("FN #{total_collaborative_fn}")

  file.puts("Total Content ")
  file.puts("TP #{total_content_tp}")
  file.puts("TN #{total_content_tn}")
  file.puts("FP #{total_content_fp}")
  file.puts("FN #{total_content_fn}")
  file.puts("Total: #{total} ")

  file.close
end


# 1, 2, 3 -> dislike
# 4, 5 -> like
def like_rating(rating)
  rating.to_i >= 4
end

# create_users
# create_items
# create_training_data
# update_recommendations

compute_precision(10)
compute_precision(5)
compute_precision(3)
compute_precision(15)

t1 = Time.now
ActsAsVotable::Vote.count
#8012
t2 = Time.now
delta = t2 - t1